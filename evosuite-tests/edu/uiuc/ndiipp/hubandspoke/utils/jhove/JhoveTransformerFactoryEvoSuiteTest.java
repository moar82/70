/*
 * This file was automatically generated by EvoSuite
 */

package edu.uiuc.ndiipp.hubandspoke.utils.jhove;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import edu.harvard.hul.ois.jhove.JhoveException;
import edu.uiuc.ndiipp.hubandspoke.utils.jhove.JhoveTransformerFactory;
import javax.xml.xpath.XPathExpressionException;
import org.junit.BeforeClass;
import org.w3c.dom.Node;

@RunWith(EvoSuiteRunner.class)
public class JhoveTransformerFactoryEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      JhoveTransformerFactory jhoveTransformerFactory0 = new JhoveTransformerFactory();
      assertNotNull(jhoveTransformerFactory0);
  }

  @Test
  public void test1()  throws Throwable  {
      try {
        JhoveTransformerFactory.getJhoveTransformer("0O%Z+qEY;N1@`");
        fail("Expecting exception: JhoveException");
      } catch(JhoveException e) {
        /*
         * Configuration file not found or not readable; use -c to specify
         */
      }
  }

  @Test
  public void test2()  throws Throwable  {
      try {
        JhoveTransformerFactory.determineMIMEType((Node) null);
        fail("Expecting exception: XPathExpressionException");
      } catch(XPathExpressionException e) {
      }
  }

  @Test
  public void test3()  throws Throwable  {
      Class<Object> class0 = JhoveTransformerFactory.getTransformerClass(":jJng'6f/V7[;fil");
      assertNull(class0);
  }
}
