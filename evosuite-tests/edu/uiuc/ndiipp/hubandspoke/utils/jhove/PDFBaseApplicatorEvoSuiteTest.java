/*
 * This file was automatically generated by EvoSuite
 */

package edu.uiuc.ndiipp.hubandspoke.utils.jhove;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.evosuite.junit.EvoSuiteRunner;
import static org.junit.Assert.*;
import edu.uiuc.ndiipp.hubandspoke.utils.jhove.PDFBaseApplicator;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import org.apache.html.dom.HTMLDocumentImpl;
import org.junit.BeforeClass;
import org.w3c.dom.Node;

@RunWith(EvoSuiteRunner.class)
public class PDFBaseApplicatorEvoSuiteTest {

  @BeforeClass 
  public static void initEvoSuiteFramework(){ 
    org.evosuite.Properties.REPLACE_CALLS = true; 
  } 


  @Test
  public void test0()  throws Throwable  {
      PDFBaseApplicator pDFBaseApplicator0 = new PDFBaseApplicator();
      HTMLDocumentImpl hTMLDocumentImpl0 = new HTMLDocumentImpl();
      try {
        pDFBaseApplicator0.parameterTransform((Node) hTMLDocumentImpl0, "/eXk*c w]G%CIAN", "/eXk*c w]G%CIAN");
        fail("Expecting exception: TransformerConfigurationException");
      } catch(TransformerConfigurationException e) {
        /*
         * Failed to compile stylesheet. 1 error detected.
         */
      }
  }
}
